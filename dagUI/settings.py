from PySide import QtGui

class ViewSettings(object):
	""" To store view options """
	def __init__(self, **kwargs):
		super(ViewSettings, self).__init__()

		self.brush = kwargs.get("brush",QtGui.QBrush(QtGui.QColor(50, 50, 50)))
		self.gridPenSmall = kwargs.get("gridPenSmall",QtGui.QPen(QtGui.QColor(44, 44, 44, 255), 0.5))
		self.gridPenLarge = kwargs.get("gridPenLarge",QtGui.QPen(QtGui.QColor(40, 40, 40, 255), 1.0))
		self.mouseWheelZoomRate = kwargs.get("mouseWheelZoomRate",0.001)
		self.title = kwargs.get("title","Node Graph View")
		self.defaultNodeBgColor = QtGui.QColor(0.2*255, 0.4*255, 0.4*255, 1.0*255)


class NodeSettings(object):
	""" To store node options """
	def __init__(self, **kwargs):
		super(NodeSettings, self).__init__()
		self.font = kwargs.get("font", QtGui.QFont('Decorative', 12)) 
		self.selectedPen = kwargs.get("selectedPen", QtGui.QPen(QtGui.QColor(255, 255, 255, 255), 1.0))
		self.unselectedPen = kwargs.get("unselectedPen", QtGui.QPen(QtGui.QColor(25, 25, 25), 1.0))
		self.maxWidth = kwargs.get("maxWidth", 80)
