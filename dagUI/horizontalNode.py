from sets import Set
from PySide import QtGui, QtCore

from baseNode import BaseNode
from port import Port


class HorizontalNode(BaseNode):

	def __init__(self, view, name="Default Name", hidePortsLabels=False, bgColor=None):
		super(HorizontalNode, self).__init__(view, name, hidePortsLabels, bgColor)

	def _computeBoundingRect(self):
		portCnt = 0

		if self.inputPortsCount > self.outputPortsCount:
			portCnt = self.inputPortsCount
		else:
			portCnt = self.outputPortsCount

		yOffset = (portCnt * Port.portSeparation) + Port.portSeparation * 2.4

		return QtCore.QRectF(0, 0, self._maxWidth, yOffset)

	def reorderInputsNodes(self, nodeIndex=0, movedNodes=Set()):

		if self.hasNoInputsConnected():
			return

		xOffset =  self._boundingRect.width() * - 1.88
		yOffset =  self._boundingRect.height() * - 0.55
		offset = QtCore.QPointF(xOffset, yOffset)

		yPortOffset = yOffset * -2

		for port in self.inputPorts:
			if port.isConnected():

				link = port.getFirstLink()
				node = link.outputPlug.port.node
				
				if node in movedNodes:
					continue
				else:
					movedNodes.add(node)
					portOffset = QtCore.QPointF(0, yPortOffset * nodeIndex)

					node.setPos(self.pos() + offset + portOffset)
					node.updateOutputLinksPaths()
					node.updateInputLinksPaths()
					nodeIndex+=1
					node.reorderInputsNodes(nodeIndex, movedNodes)

	def paint(self, painter, option, widget):
		path = QtGui.QPainterPath()
		path.addRoundedRect(self._boundingRect, self._cornerRadius, self._cornerRadius)
		if self.isSelected():
			painter.setPen(BaseNode._settings.selectedPen)
		else:
			painter.setPen(BaseNode._settings.unselectedPen)
		painter.setBrush(self.bgColor)
		painter.drawPath(path)
