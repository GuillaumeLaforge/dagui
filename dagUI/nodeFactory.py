from PySide import QtGui, QtCore

from horizontalNode import HorizontalNode
from verticalNode import VerticalNode

class NodeFactory(object):

	nodeClasses = { "HorizontalNode": HorizontalNode,
		"VerticalNode": VerticalNode
	}

	def __init__(self):
		super(NodeFactory, self).__init__()

	@classmethod
	def create(cls, view, name, nodeType, hidePortsLabels=False, bgColor=None):
		nodeClass = cls.nodeClasses.get(nodeType)
		return nodeClass(view, name, hidePortsLabels, bgColor)
