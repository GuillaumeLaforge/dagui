import uuid
from sets import Set
from PySide import QtGui, QtCore

from dagUIExceptions import DagUIAbstractClassException
from settings import NodeSettings		
from label import Label
from port import Port

class BaseNode(QtGui.QGraphicsItem):

	_settings = NodeSettings()

	@classmethod
	def setSettings(cls, **kwargs):
		cls._settings = NodeSettings(**kwargs)

	def __init__(self, 
				view, 
				name="Default Name", 
				hidePortsLabels=False, 
				bgColor=None
		):

		if self.__class__.__name__ == "BaseNode":
			raise DagUIAbstractClassException("Can't instanciate BaseNode directly")

		super(BaseNode, self).__init__()
		
		view.scene().addItem(self)

		self.setFlag(QtGui.QGraphicsItem.ItemIsSelectable)
		
		self.__bgColor = bgColor
		self.__uuid = uuid.uuid4()
		self.__view = view
		self.__name = name
		self.__startMoving = False
		self.__hasMoved = False
		self.__inputPorts = []
		self.__outputPorts = []
		settings = view.getNodeSettings()
		self._maxWidth = settings.maxWidth
		self._maxHeight = 25
		self._cornerRadius = 0
		self._boundingRect = self._computeBoundingRect()
		self._hidePortsLabels = hidePortsLabels
		self._label = Label(self, name, settings.font, self._boundingRect.width(), 16)

	@property
	def name(self):
		return self.__name
		
	@property
	def bgColor(self):
		return self.__bgColor

	@property
	def inputPortsCount(self):
		return len(self.__inputPorts)

	@property
	def outputPortsCount(self):
		return len(self.__outputPorts)

	@property
	def inputPorts(self):
		return self.__inputPorts

	@property
	def outputPorts(self):
		return self.__outputPorts

	def _computeBoundingRect(self):
		return QtCore.QRectF()

	def addPort(self, name, cnxType="I"):
		port = Port(self, name, cnxType, self._hidePortsLabels, self.__view.orientation)
		if cnxType == "I":
			self.__inputPorts.append(port)
		else:
			self.__outputPorts.append(port)

		self._boundingRect = self._computeBoundingRect()		
		return port
		
	def hasNoOutputsConnected(self):
		for port in self.outputPorts:
			if port.isConnected():
				return False
		return True

	def hasNoInputsConnected(self):
		for port in self.inputPorts:
			if port.isConnected():
				return False
		return True

	def disconnectAll(self):
		for port in self.__inputPorts:
			port.disconnectAll()

		for port in self.__outputPorts:
			port.disconnectAll()

	def copy(self):
		copie = self.__class__(self.__view, self.__name)

		for port in self.__inputPorts:
			copie.addPort(port.name, cnxType="I")

		for port in self.__outputPorts:
			copie.addPort(port.name, cnxType="O")

		x = self.pos().x() + 40
		y = self.pos().y() + 40
		pos = QtCore.QPointF(x, y)
		copie.setPos(pos)
		return copie

	def updateOutputLinksPaths(self):
		for port in self.__outputPorts:
			portScenePos = port.plug.scenePos()
			for link in port.plug.links:
				link.updatePath(portScenePos, link.endPos)

	def updateInputLinksPaths(self):
		for port in self.__inputPorts:
			portScenePos = port.plug.scenePos()
			for link in port.plug.links:
				link.updatePath(link.startPos, portScenePos)

	def reorderInputsNodes(self, nodeIndex=0, movedNodes=Set()):
		pass

	def boundingRect(self):
		return self._boundingRect

	def paint(self, painter, option, widget):
		pass

	def mousePressEvent(self, event):
		if event.button() is QtCore.Qt.MouseButton.RightButton:
			super(BaseNode,self).mousePressEvent(event)
		else:
			if event.button() is QtCore.Qt.MouseButton.LeftButton \
			or (event.button() is QtCore.Qt.MouseButton.MiddleButton and self.__view.shiftKeyState):
				self.__dragStartPos = QtCore.QPointF(event.pos())
				self.__startMoving = True
		event.accept()

	def mouseMoveEvent(self, event):
		if self.__startMoving and not self.__view.panning:
			self.__hasMoved = True

			selectedNodes = self.scene().selectedItems()
			if not self.isSelected():
				selectedNodes.append(self)

			dragDelta = event.pos() - self.__dragStartPos
			
			for node in selectedNodes:
				pos = node.pos() + dragDelta
				prevNodePos = node.pos()
				nodePosDelta = pos - prevNodePos

				node.setPos(pos)
				node.updateOutputLinksPaths()
				node.updateInputLinksPaths()

			super(BaseNode,self).mouseMoveEvent(event)

	def mouseReleaseEvent(self, event):
		self.__startMoving = False
		if not self.__hasMoved:
			if self.__view.ctrlKeyState is not True:
				self.scene().clearSelection()
			if self.isSelected():
				self.setSelected(False)
			else:
				self.setSelected(True)
		self.__hasMoved = False

	def mouseDoubleClickEvent(self, event):
		self.scene().clearSelection()
		self.setSelected(True)

