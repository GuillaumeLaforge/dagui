from sets import Set
import uuid
from PySide import QtGui, QtCore

from link import Link
from dagUIExceptions import DagUIPortConnectionException


class Plug(QtGui.QGraphicsItem):
	""" A port's plug to be connected through a Link object to an other plug """
	def __init__(self, port, radius=4.0, color=QtGui.QColor(0, 0, 0)):

		super(Plug, self).__init__(port)
		super(Plug, self).setAcceptHoverEvents(True)

		self.__uuid = uuid.uuid4()
		self.__port = port
		self.__color = color
		self.radius = radius
		self.__boundingRect = QtCore.QRectF(-self.radius, -self.radius, self.radius * 2, self.radius * 2)
		self.__pen = QtGui.QPen(QtCore.Qt.NoPen)
		self.__links = Set()
		self.__newLink = None

	def __eq__(self, other):
		return self.__uuid == other.__uuid

	@property
	def name(self):
		return "%s (type: %s)" % (self.__port.name, self.cnxType)
	
	@property
	def fullName(self):
		return "%s.%s" % (self.__port.node.name, self.name)

	@property
	def links(self):
		return self.__links

	@property
	def port(self):
	    return self.__port
	
	@property
	def cnxType(self):
		return self.__port.cnxType

	def _createLink(self, plug=None):
		""" Create a new link from self to plug (if provided). Else create an incomplete link from this plug.
			Such incomplete link is only usefull when the connection is done interactively from the View UI

			plug (Plug): if provided, the link will be referenced by the plug
		"""

		link = Link(self)

		if plug:
			if self.cnxType == "O":
				link.setInputPlug(plug)
			else:
				link.setOutputPlug(plug)

		return link

	def connectTo(self, plug, interactive=False):
		""" Connect self to an other one.
			If self's port is an output, plug's port must be an input and vice versa 

			interactive (bool): set to True when connecting from a mouse events
		"""
		if (self.cnxType == "O" and plug.cnxType == "O") or (self.cnxType == "I" and plug.cnxType == "I"):
			if interactive:
				self.scene().removeItem(self.__newLink)
				self.__newLink = None
			raise DagUIPortConnectionException("Ports got same connection type! Can't connect, operation aborted")

		# If we are not called from a mouse move event
		if interactive:
			if self.cnxType == "O":
				self.__newLink.setInputPlug(plug)
			else:
				self.__newLink.setOutputPlug(plug)
		else:
			self.__newLink = self._createLink(plug)

		if self.cnxType == "O":
			self.__newLink.updatePath(self.scenePos(), plug.scenePos())
		else:
			self.__newLink.updatePath(plug.scenePos(), self.scenePos())

		self.__links.add(self.__newLink)
		plug.__links.add(self.__newLink)
		self.__newLink = None
		
	def _removeLinkFromList(self, link):
		if link in self.__links:
			self.__links.remove(link)
			self.scene().removeItem(link)

	def disconnectFrom(self, plug):
		""" Break connexion between self and plug. 

			plug (Plug): the plug to diconnect from
		"""

		linkToRemove = None
		for link in self.__links:
			if (self.cnxType == "O" and link.inputPlug == plug) or (self.cnxType == "I" and link.outputPlug == plug):
					linkToRemove = link
					break

		# Do nothing if there is nothing to disconnect
		if linkToRemove is None:
			return

		self.__links.remove(linkToRemove)

		linkToRemove = None
		for link in plug.__links:
			if (plug.cnxType == "I") and (link.outputPlug == self) or (plug.cnxType == "O") and (link.inputPlug == self):
				linkToRemove = link
				break

		plug.__links.remove(linkToRemove)			

		self.scene().removeItem(linkToRemove)

	def removeLinks(self):
		if self.cnxType == "I":
			for link in self.__links:
				link.outputPlug._removeLinkFromList(link)
		else:
			for link in self.__links:
				link.inputPlug._removeLinkFromList(link)

			self.__links = Set()


	def paint(self, painter, option, widget):
		""" Draw the plug as a circle """
		painter.setBrush(QtGui.QBrush(self.__color))
		painter.setPen(self.__pen)
		painter.drawEllipse(self.__boundingRect)

	def boundingRect(self):
		return self.__boundingRect

	def mousePressEvent(self, event):
		""" Create a Link object if the plug's port is an output """
		if event.button() is QtCore.Qt.MouseButton.RightButton:
			self.removeLinks()
			event.ignore()
		else:
			self.__newLink = self._createLink()
			self.__mousePressPos = event.pos()

	def mouseMoveEvent(self, event):
		""" Add a finder to the end of the link. Update the link path """
		if self.__newLink:
			self.__newLink.addFinder()
			startPos = self.mapToScene(self.__mousePressPos)
			endPos = self.mapToScene(event.pos())

			revert = self.cnxType == "I"
			self.__newLink.updatePath(startPos, endPos, revert)

	def mouseReleaseEvent(self, event):
		""" If the "link finder" find an input plug, keep the link and connect to the plug
			else, delete the link
		"""
		if self.__newLink and self.__newLink.finder:

			for item in self.__newLink.finder.collidingItems(QtCore.Qt.IntersectsItemBoundingRect):
				if isinstance(item, Plug):
					self.connectTo(item, interactive=True)
					return

			self.scene().removeItem(self.__newLink)
			self.__newLink = None

	def hoverEnterEvent(self, event):
		""" Display the plug bold green  """
		self.__pen = QtGui.QPen(QtCore.Qt.SolidLine)
		self.__pen.setWidth(2)
		self.__pen.setColor(QtGui.QColor(100, 255, 0))
		super(Plug, self).hoverEnterEvent(event)

	def hoverLeaveEvent(self, event):
		""" Return to plug standard display """
		self.__pen = QtGui.QPen(QtCore.Qt.NoPen)
		self.__pen.setWidth(0)
		super(Plug, self).hoverLeaveEvent(event)

