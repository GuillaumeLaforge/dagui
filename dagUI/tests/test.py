# python -m unittest discover dagUI/tests/

import sys
import unittest

from PySide import QtGui, QtCore

from dagUI import HorizontalNode, View


class QApplicationTestCase(unittest.TestCase):
	'''Helper class to provide QApplication instances'''

	def setUp(self):
		'''Creates the QApplication instance'''

		# Simple way of making instance a singleton
		super(QApplicationTestCase, self).setUp()
		self.app = QtGui.QApplication.instance()
		if self.app is None:
			args = []
			if hasattr(sys, 'argv'):
				args = sys.argv
			self.app = QtGui.QApplication(args)
		
	def tearDown(self):
		'''Deletes the reference owned by self'''
		del self.app
		super(QApplicationTestCase, self).tearDown()


class TestPortConnection(QApplicationTestCase):

	def setUp(self):
		super(TestPortConnection, self).setUp()

		self.scene = QtGui.QGraphicsScene(QtCore.QRect(-10000, -10000, 20000, 20000))
		self.view = View(self.scene)
		self.nodeA = self.view.addNode("A")
		self.nodeB = self.view.addNode("B")
		self.nodeC = self.view.addNode("C")

		self.outPort = self.nodeA.addPort("out", cnxType="O")
		self.inPort = self.nodeB.addPort("in", cnxType="I")



	def tearDown(self):
		super(TestPortConnection, self).tearDown()

	def test_connectOutPortToInPort(self):
		"""Test port connection from A.out to B.in."""
		self.outPort.connectTo(self.inPort)

		self.assertTrue(self.outPort.isConnected())
		self.assertTrue(self.inPort.isConnected())
		self.assertEqual(self.outPort.connectionsCount(), 1)
		self.assertEqual(self.inPort.connectionsCount(), 1)

	def test_disconnectOutPortFromInPort(self):
		"""Test port disconnection from A.out to B.in."""
		self.outPort.disconnectFrom(self.inPort)

		self.assertFalse(self.outPort.isConnected())
		self.assertFalse(self.inPort.isConnected())
		self.assertEqual(self.outPort.connectionsCount(), 0)
		self.assertEqual(self.inPort.connectionsCount(), 0)

	def test_connectInPortToOutPort(self):
		"""Test port connection from b.in to A.out.""" 
		self.inPort.connectTo(self.outPort)

		self.assertTrue(self.outPort.isConnected())
		self.assertTrue(self.inPort.isConnected())
		self.assertEqual(self.outPort.connectionsCount(), 1)
		self.assertEqual(self.inPort.connectionsCount(), 1)

	def test_disconnectInPortFromOutPort(self):
		"""Test port disconnection from B.in to A.out."""
		self.inPort.disconnectFrom(self.outPort)

		self.assertFalse(self.outPort.isConnected())
		self.assertFalse(self.inPort.isConnected())
		self.assertEqual(self.outPort.connectionsCount(), 0)
		self.assertEqual(self.inPort.connectionsCount(), 0)

	def test_disconnectAll(self):
		"""Create a new node with many inputs ports all connected to node A output port."""
		inputsCnt = 100

		for i in range(0, inputsCnt):
			port = self.nodeC.addPort("in_%s" % i, cnxType="I")
			self.outPort.connectTo(port)

		self.assertEqual(self.nodeC.inputPortsCount, inputsCnt)
		self.assertEqual(self.outPort.connectionsCount(), inputsCnt)
		self.outPort.disconnectAll()
		self.assertEqual(self.outPort.connectionsCount(), 0)


if __name__ == "__main__":
    unittest.main()

