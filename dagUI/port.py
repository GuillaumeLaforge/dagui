from PySide import QtGui, QtCore

from label import Label
from plug import Plug


class Port(QtGui.QGraphicsWidget):

	portSeparation = 13.0
	__font = QtGui.QFont('Decorative', 10)
	__fontMetrics = QtGui.QFontMetrics(__font)
	__portRadius = 4.0

	def __init__(self, node, name, cnxType="I", hideLabel=False, orientation="Horizontal"):
		super(Port, self).__init__(node)

		self.setCursor(QtCore.Qt.ArrowCursor)
		self.setFlag(QtGui.QGraphicsItem.ItemSendsScenePositionChanges)
		
		self.__index = None
		self.__node = node
		self.__name = name
		self.__label = Label(self, self.__name, self.__font, self.__node._computeBoundingRect().width() * 0.5, 12)
		if hideLabel:
			self.__label.hide()
		self.__plug = Plug(self, Port.__portRadius)
		self.__plugType = cnxType
		
		self.__orientation = orientation
		self._setPosition(self.__orientation)

	@property
	def node(self):
	    return self.__node
	
	@property
	def plug(self):
		return self.__plug
	
	@property
	def index(self):
		if self.__index is None:
			if self.__plugType == "I":
				self.__index = self.__node.inputPortsCount
			else:
				self.__index = self.__node.outputPortsCount
		return self.__index

	@property
	def cnxType(self):
		return self.__plugType

	@property
	def name(self):
	    return self.__name

	@property
	def fullName(self):
	    return "%s.%s" % (self.__node.name, self.name)

	@property
	def label(self):
	    return self.__label
	
	@property
	def orientation(self):
	    return self.__orientation
	
	def getFirstLink(self):
		""" Accessing directly the first link is usefull when we've got an input port as it gives
			easy access to the output port connected to it
		"""
		return next(iter(self.__plug.links))

	def _setPosition(self, orientation):
		if orientation == "Horizontal":
			nodeLabelHeight = self.__label.height * 2.0
			posY = Port.portSeparation
			posY += (self.index * Port.portSeparation)
			if self.__plugType == 'I':
				self.__plug.setPos(0, nodeLabelHeight + posY)
				self.__label.setPos(Port.__portRadius, nodeLabelHeight - (self.__label.height * 0.5) \
					+ posY - Port.__portRadius)
			else:
				posX = self.__node.boundingRect().width()
				self.__plug.setPos(posX, nodeLabelHeight + posY)
				self.__label.setPos(posX - Port.__portRadius * 3 - self.__label.width, \
					nodeLabelHeight - (self.__label.height * 0.5) + posY - Port.__portRadius)


	def updateHorizontalPosition(self):
		posX = self.__node.boundingRect().width() / (2 * (self.index + 1))
		if self.__plugType == "I":
			self.__plug.setPos(posX, 0)
		else:
			posY = self.__node.boundingRect().height()
			self.__plug.setPos(posX, posY)


	def connectTo(self, inPort):
		self.plug.connectTo(inPort.plug)

	def disconnectFrom(self, inPort):
		self.plug.disconnectFrom(inPort.plug)

	def disconnectAll(self):
		self.plug.removeLinks()

	def isConnected(self):
		if len(self.plug.links) > 0:
			return True
		return False

	def connectionsCount(self):
		return len(self.plug.links)



