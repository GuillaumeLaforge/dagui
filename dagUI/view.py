import sys
from sets import Set

from PySide import QtGui, QtCore

from dagUIExceptions import DagUIViewException
from settings import ViewSettings, NodeSettings
from nodeFactory import NodeFactory
from baseNode import BaseNode
from port import Port
from link import Link

		
class View(QtGui.QGraphicsView):
	"""The NodeGraph view"""

	__settings = ViewSettings()
	__nodesSettings = NodeSettings()

	@classmethod
	def setSettings(cls, **kwargs):
		cls.__settings = ViewSettings(**kwargs)

	@classmethod
	def setNodeSettings(cls, **kwargs):
		cls.__nodesSettings = NodeSettings(**kwargs)

	@classmethod
	def getNodeSettings(cls):
		return cls.__nodesSettings

	def __init__(self, scene, parent=None, title="Node Graph", orientation="Horizontal", nodeStyle="Classic"):
		super(View, self).__init__(scene, parent)
		self.setWindowTitle(title)
		self.setBackgroundBrush(View.__settings.brush)
		  
		# Hide Scroll bars
		self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
		self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

		# To transform the scene using the mouse as the anchor point
		self.setResizeAnchor(QtGui.QGraphicsView.AnchorUnderMouse)

		# Antialiasing
		self.setRenderHint(QtGui.QPainter.Antialiasing)
		self.setRenderHint(QtGui.QPainter.TextAntialiasing)    

		# navigation / selection
		self.setDragMode(QtGui.QGraphicsView.RubberBandDrag)

		self.__nodefactory = NodeFactory()
		self.__orientation = orientation
		self.__nodeStyle = nodeStyle

		self.__panning = False
		self.__mousePressed = False
		self.__mouseWheelZoomRate = 0.001
		self.__lastPanPoint = QtCore.QPointF()
		self.__center = QtCore.QPointF()

		self.__ctrlKeyState = False
		self.__shiftKeyState = False

		self.__editable = True

		def selectionChangedCallback():
			pass

		self.scene().selectionChanged.connect(selectionChangedCallback)

	@property
	def orientation(self):
		return self.__orientation
	
	@property
	def nodeStyle(self):
		return self.__nodeStyle

	@property
	def ctrlKeyState(self):
		return self.__ctrlKeyState

	@property
	def shiftKeyState(self):
		return self.__shiftKeyState

	@property
	def panning(self):
		return self.__panning

	def lock(self):
		self.__editable = False

	def unlock(self):
		self.__editable = True

	def frameAllNodes(self):
		boundingRect = self.scene().itemsBoundingRect().adjusted(-50, -50, 50, 50)
		self.fitInView(boundingRect, QtCore.Qt.KeepAspectRatio)
		self.__center = (boundingRect.topLeft() + boundingRect.bottomRight()) * 0.5

	def frameSelectedNodes(self):
		boundingRect = QtCore.QRectF()
		selectedNodes = self.scene().selectedItems()
		if len(selectedNodes) > 0:
			for node in selectedNodes:
				boundingRect = boundingRect.united(node.sceneBoundingRect()).adjusted(-50, -50, 50, 50)
		else:
			boundingRect = self.scene().itemsBoundingRect().adjusted(-50, -50, 50, 50)
		self.fitInView(boundingRect, QtCore.Qt.KeepAspectRatio)

		self.__center = (boundingRect.topLeft() + boundingRect.bottomRight()) * 0.5

	def reorderGraph(self):
		terminalNodes = Set()
		for item in self.scene().items():
			if isinstance(item, BaseNode):
				if not item.outputPorts or item.hasNoOutputsConnected():
					terminalNodes.add(item)

		for node in terminalNodes:			
			index = 0
			reordered = Set()
			node.reorderInputsNodes(index, reordered)

	def addNode(self, name, bgColor=None):
		node = None
		
		if bgColor == None:
			bgColor = View.__settings.defaultNodeBgColor
		if self.__orientation == "Horizontal" and self.__nodeStyle == "Classic":
			node = self.__nodefactory.create(self, name, "HorizontalNode", bgColor=bgColor)
		elif self.__orientation == "Vertical" and self.__nodeStyle == "Classic":
			node = self.__nodefactory.create(self, name, "VerticalNode", hidePortsLabels=True, bgColor=bgColor)
		else:
			raise DagUIViewException("Only 'Horizontal Classic' style implemented!")
		return node

	def deleteNode(self, node):
		node.disconnectAll()
		self.scene().removeItem(node)

	def drawBackground(self, painter, rect):
		super(View,self).drawBackground(painter, rect)
	
		# Draw fine grid
		gridSize = 30
		rectTop = rect.top()
		rectLeft = rect.left()
		left = int(rectLeft) - (int(rectLeft) % gridSize)
		top = int(rectTop) - (int(rectTop) % gridSize)

		# Draw vertical fine lines 
		gridLines = [] 
		painter.setPen(View.__settings.gridPenSmall)
		x = float(left)
		rectRight = rect.right()
		while x < float(rectRight):
			gridLines.append(QtCore.QLineF( x, rectTop, x, rect.bottom() ))
			x += gridSize
		painter.drawLines(gridLines)

		# Draw horizontal fine lines 
		gridLines = [] 
		y = float(top)
		while y < float(rect.bottom()):
			gridLines.append(QtCore.QLineF( rectLeft, y, rectRight, y ))
			y += gridSize
		painter.drawLines(gridLines)

		# Draw thick grid
		gridSize = 30 * 10
		left = int(rectLeft) - (int(rectLeft) % gridSize)
		top = int(rectTop) - (int(rectTop) % gridSize)

		# Draw vertical thick lines 
		gridLines = [] 
		painter.setPen(View.__settings.gridPenLarge)
		x = left
		while x < rectRight:
			gridLines.append(QtCore.QLineF( x, rectTop, x, rect.bottom() ))
			x += gridSize
		painter.drawLines(gridLines)

		# Draw horizontal thick lines 
		gridLines = [] 
		y = top
		while y < rect.bottom():
			gridLines.append(QtCore.QLineF( rectLeft, y, rectRight, y ))
			y += gridSize
		painter.drawLines(gridLines)

	def mousePressEvent(self, event):
		if not self.__editable:
			event.ignore()
			return

		self.__mousePressed = True
		if (event.button() is QtCore.Qt.MouseButton.MiddleButton) or (sys.platform == "darwin" and self.__ctrlKeyState):
			self.setCursor(QtCore.Qt.OpenHandCursor)
			self.__panning = True

		elif event.button() is QtCore.Qt.MouseButton.RightButton:
			contextMenu = QtGui.QMenu(self)

			pos = self.mapToScene(event.pos())
			item = self.scene().itemAt(pos)
			node = None
			if isinstance(item, Port):
				node = item.node
			elif isinstance(item, BaseNode):
				node = item

			if node:
				contextMenu.addAction("Duplicate node: %s" % node.name).triggered.connect(node.copy)
				contextMenu.addAction("Delete node: %s" % node.name).triggered.connect(lambda: self.deleteNode(node))

			selectedNodes = self.scene().selectedItems()
			if len(selectedNodes) > 0:
				def __reorder():
					for node in selectedNodes:
						index = 0
						reordered = Set()
						node.reorderInputsNodes(index, reordered)					

				contextMenu.addAction("Re-order Graph").triggered.connect(__reorder)

			contextMenu.popup(event.globalPos())

		self.__lastPanPoint = event.pos()
		super(View, self).mousePressEvent(event)

	def wheelEvent(self, event):
		"""invokes when the mousewheel is turned"""
		if not self.__mousePressed:
			zoomFactor = 1.0 + event.delta() * self.__mouseWheelZoomRate
			transform = self.transform()
			transform.scale(zoomFactor, zoomFactor)

			if transform.m22() > 0.01: # To avoid negative scalling as it would flip the graph
				self.setTransform(transform)

	def mouseMoveEvent(self, event):
		if self.__panning:
			delta = self.mapToScene(self.__lastPanPoint) - self.mapToScene(event.pos())
			self.__lastPanPoint = event.pos()
			self.__center += delta
			self.centerOn(self.__center);
		else:
			super(View, self).mouseMoveEvent(event)

	def mouseReleaseEvent(self, event):
		self.__mousePressed = False
		if self.__panning:
			self.setCursor(QtCore.Qt.ArrowCursor)
			self.__panning = False
			self.__lastPanPoint = self.__center
		super(View, self).mouseReleaseEvent(event)
	
	def keyPressEvent(self, event):
		if event.key() == QtCore.Qt.Key_Control:
			self.__ctrlKeyState = True
		if event.key() == QtCore.Qt.Key_Shift:
			self.__shiftKeyState = True
		elif event.key() == QtCore.Qt.Key_F:
			self.frameSelectedNodes()
		elif event.key() == QtCore.Qt.Key_A:
			self.frameAllNodes()
		super(View,self).keyPressEvent(event)

	def keyReleaseEvent(self,event):
		self.__ctrlKeyState = False
		self.__shiftKeyState = False

