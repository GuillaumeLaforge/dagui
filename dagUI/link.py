from collections import namedtuple
from PySide import QtGui, QtCore


class LinkSettings(object):
	""" To store link options """
	def __init__(self, **kwargs):
		super(LinkSettings, self).__init__()
		self.lineWidth = kwargs.get("lineWidth", 2.0)
		self.tangent = kwargs.get("tangent",45.0)
		self.color = kwargs.get("color",QtGui.QColor(0, 0, 0))
		

class Link(QtGui.QGraphicsPathItem):
	""" To draw a curve between connected ports """
	
	def __init__(self, plug):

		super(Link, self).__init__(parent=None, scene=plug.scene())

		self.__settings = LinkSettings()

		self.__outputPlug = None
		self.__inputPlug = None

		if plug.cnxType == "O":
			self.__outputPlug = plug
		else:
			self.__inputPlug = plug

		self.__finder = None
		self.__shape = QtGui.QGraphicsPathItem(self)
		self.__shape.setPen(QtGui.QPen(self.__settings.color, self.__settings.lineWidth))
		self.__startPos = None
		self.__endPos = None

	@property
	def startPos(self):
		return self.__startPos
	
	@property
	def endPos(self):
		return self.__endPos
			
	@property
	def outputPlug(self):
		""" Return the plug connected to the beginning of the link """
		return self.__outputPlug
	
	@property
	def inputPlug(self):
		""" Return the plug connected to the end of the link """
		return self.__inputPlug

	def setInputPlug(self, plug):
		""" Store the plug used to connect the link to an input port """
		plug.removeLinks()
		self.__inputPlug = plug
		if self.__finder:
			self.scene().removeItem(self.__finder)

	def setOutputPlug(self, plug):
		""" Store the plug used to connect the link to an input port """
		self.inputPlug.removeLinks()
		self.__outputPlug = plug
		if self.__finder:
			self.scene().removeItem(self.__finder)

	@property
	def finder(self):
		""" Return a finder object or None when called on a link already connected """
		return self.__finder

	def addFinder(self):
		""" Create a finder object used to detect a plug to connect the link to """
		if not self.__finder:
			radius = 0.0
			if self.__outputPlug:
				radius = self.__outputPlug.radius * 1.5
			else:
				radius = self.__inputPlug.radius * 1.5

			self.__finder = QtGui.QGraphicsEllipseItem(QtCore.QRectF(-radius, -radius, radius * 2, radius * 2), self)

	def updatePath(self, start, end, revert=False):
		""" Set the curve shape from a start and end QPointF's """


		plug = self.inputPlug
		if not plug:
			plug = self.outputPlug

		isVertical = False
		if plug.port.orientation == "Vertical":
			isVertical = True

		# When the curve length is greater than cutoffMax, self.__settings.tangent is used,
		# if not, the tangent value is remaped to avoid uggly curves when two connected 
		# nodes are very close to each others
		length = self.__shape.path().length()
		cutoffMax = 100.0
		cutoffMin = 10.0
		tangent = self.__settings.tangent
		if length < cutoffMax and length > cutoffMin:
			weight = length / cutoffMax
			tangent *= weight

		if revert:
			tangent *= -1

		# Set the curve shape
		path = QtGui.QPainterPath()
		path.moveTo(start)
		if isVertical:
			direction = QtCore.QPointF(0, tangent)
			path.cubicTo(start + direction, end - direction, end)
		else:
			direction = QtCore.QPointF(tangent, 0)
			path.cubicTo(start + direction, end - direction, end)

		self.__shape.setPath(path)
		self.__startPos = start
		self.__endPos = end
		
		# move the finder to the start or end of the curve
		if self.__finder:
			self.__finder.setPos(end)
