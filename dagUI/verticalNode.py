from sets import Set
from PySide import QtGui, QtCore

from baseNode import BaseNode
from port import Port


class VerticalNode(BaseNode):

	def __init__(self, view, name="Default Name", hidePortsLabels=False, bgColor=None):
		super(VerticalNode, self).__init__(view, name, hidePortsLabels, bgColor)

		self._centerLabel()

	def _computeBoundingRect(self):
		portCnt = 0

		if self.inputPortsCount > self.outputPortsCount:
			portCnt = self.inputPortsCount
		else:
			portCnt = self.outputPortsCount

		xOffset = (portCnt * Port.portSeparation) + Port.portSeparation * 2.4
		if xOffset > self._maxWidth + 10:
			self._maxWidth = xOffset

		self._setPortsPositions()

		return QtCore.QRectF(0, 0, self._maxWidth, self._maxHeight)

	def _centerLabel(self):
		x = (self._boundingRect.width() * 0.5) - self._label.width * 0.5
		y = self._boundingRect.height() * 0.1
		self._label.setPos(x - 5, y)

	def _setPortsPositions(self):
		if self.inputPortsCount > 0:
			edgeLength = self._boundingRect.width() / self.inputPortsCount
			edgeLength -= edgeLength / (self.inputPortsCount + 1)
			posx = 0
			for port in self.inputPorts:
				posx += edgeLength
				port.plug.setPos(posx, 0)

		if self.outputPortsCount > 0:
			posy = self._boundingRect.height()
			edgeLength = self._boundingRect.width() / self.outputPortsCount
			edgeLength -= edgeLength / (self.outputPortsCount + 1)
			posx = 0
			for port in self.outputPorts:
				posx += edgeLength
				port.plug.setPos(posx, posy)

	def reorderInputsNodes(self, nodeIndex=0, movedNodes=Set()):

		if self.hasNoInputsConnected():
			return

		thisPosx = self.pos().x()
		nodeWidth =  self._boundingRect.width() * - 0.8
		nodeHeigth =  self._boundingRect.height()
		portsMidle = self.inputPortsCount / 2
		scale = 230
		xOffsets = [((x - portsMidle) * 0.5) * scale for x in range(0, self.inputPortsCount)]

		for index, port in enumerate(self.inputPorts):
			if port.isConnected():			
				node = port.getFirstLink().outputPlug.port.node
				
				if node in movedNodes:
					continue
				else:
					movedNodes.add(node)
				
					node.setPos((thisPosx + nodeWidth * 0.5) + xOffsets[index], self.pos().y() + nodeHeigth * -3.5)

					node.updateOutputLinksPaths()
					node.updateInputLinksPaths()
					nodeIndex+=1
					node.reorderInputsNodes(nodeIndex, movedNodes)

	def copy(self):
		node = super(VerticalNode, self).copy()
		for port in node.inputPorts + node.outputPorts:
			port.label.hide()
		return node

	def paint(self, painter, option, widget):
		path = QtGui.QPainterPath()
		path.addRoundedRect(self._boundingRect, self._cornerRadius, self._cornerRadius)
		
		if self.isSelected():
			painter.setPen(BaseNode._settings.selectedPen)
		else:
			painter.setPen(BaseNode._settings.unselectedPen)
		painter.setBrush(self.bgColor)
		painter.drawPath(path)
