
class DagUIException(Exception):
    '''Base class for dagUI Exceptions, only used to except any dagUI error, never raised'''
    
class DagUIAbstractClassException(DagUIException):
    pass

class DagUIPortConnectionException(DagUIException):
    pass

class DagUIViewException(DagUIException):
    pass
