from PySide import QtGui, QtCore


class Label(QtGui.QGraphicsTextItem):
	
	def __init__(self, parent, label, font, maxWidth, offset):
		super(Label, self).__init__(parent)

		self.__text = label
		self.setFont(font)
		self.__maxWidth = maxWidth
		self.__offset = offset
		self.setPos(0, 0)
		self.setToolTip(self.__text)
		self.setLabel()

	def setLabel(self):
		label = str(self.__text)
		while QtGui.QFontMetrics(self.font()).width(label) + self.__offset > self.__maxWidth:
			if label.endswith('...'):
				label = label[:-3]
			label = label[:-1]+'...'
		self.setPlainText(label)

	@property
	def width(self):
		label = str(self.toPlainText())
		return QtGui.QFontMetrics(self.font()).width(label)

	@property
	def height(self):
		return QtGui.QFontMetrics(self.font()).height()

