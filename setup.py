from setuptools import setup

setup(name='dagUI',
	version='0.1',
	description='A PySide QGraphicsView to display a directed acyclic node graph',
	url='',
	author='Guillaume Laforge',
	author_email='guillaume.laforge.3d@gmail.com',
	license='BSD-3',
	packages=['dagUI'],
	install_requires=[
          'PySide',
	],	
	zip_safe=False)
