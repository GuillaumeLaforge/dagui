#!/usr/bin/env python

import sys
from PySide import QtGui, QtCore
from dagUI.view import View

# Change default settings on view
View.setSettings(brush=QtGui.QBrush(QtGui.QColor(60, 60, 65)),
	mouseWheelZoomRate=0.01
)

# Change default settings on nodes
View.setNodeSettings(font=QtGui.QFont('Decorative', 14), maxWidth=165)

nodeSettings = View.getNodeSettings()

app = QtGui.QApplication(sys.argv)

# Create scene, view and nodes instances

scene = QtGui.QGraphicsScene(QtCore.QRect(-10000, -10000, 20000, 20000))

view = View(scene, 
	parent=None, 
	title="My Perfect Workflow",
	orientation="Vertical"
)

# Un-comment if you don't want the user to edit the graph
# view.lock()

a = view.addNode("Open", bgColor=QtGui.QColor(0.1*255, 0.3*255, 0.6*255, 1.0*255))
outA = a.addPort("out", cnxType="O")

b = view.addNode("Custom Action(s)")
inB = b.addPort("in", cnxType="I")
outB = b.addPort("out", cnxType="O")

c = view.addNode("Fixed Workflow", bgColor=QtGui.QColor(0.2*255, 0.4*255, 0.6*255, 1.0*255))
inC = c.addPort("in", cnxType="I")
outC = c.addPort("out1", cnxType="O")

endNode = view.addNode("Custom Action(s)")
inEnd1 = endNode.addPort("in1", cnxType="I")
outEnd1 = endNode.addPort("in1", cnxType="O")

doneNode = view.addNode("Done", bgColor=QtGui.QColor(0.1*255, 0.3*255, 0.6*255, 1.0*255))
inDone = doneNode.addPort("in1", cnxType="I")

outA.connectTo(inB)
outB.connectTo(inC)
outC.connectTo(inEnd1)
outEnd1.connectTo(inDone)

view.reorderGraph()

view.show()

# On OSX, the app appear behind others if not calling raise_. 
if sys.platform == "darwin":
	view.raise_()

sys.exit(app.exec_())