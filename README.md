# dagUI
A PySide based node graph UI

dagUI was part of my [*Nanode* project](https://vimeo.com/47944043), a node graph editor to build 3D scenes.
As the usage of such node grah UI is not restricited to 3D, I moved the UI part in its own package.


### To create a simple node graph UI

```python
import sys
from PySide import QtGui, QtCore

from dagUI.view import View

# Lets change a couple of settings on the View and Nodes
View.setSettings(brush=QtGui.QBrush(QtGui.QColor(60, 60, 65)),
	mouseWheelZoomRate=0.01
)

View.setNodeSettings(font=QtGui.QFont('Decorative', 14))

# Now that dagUI is configured, lets create a sample QtGui app and create our dagUI widget
app = QtGui.QApplication(sys.argv)
scene = QtGui.QGraphicsScene(QtCore.QRect(-10000, -10000, 20000, 20000))

view = View(scene, 
	parent=None, 
	title="Hello Vertical Graph",
	orientation="Vertical"
)

# Create a connect a couple of nodes
a = view.addNode("A", bgColor=QtGui.QColor(0.1*255, 0.3*255, 0.6*255, 1.0*255))
inA = a.addPort("in", cnxType="I")
outA = a.addPort("out", cnxType="O")

b = view.addNode("B")
inB = b.addPort("in", cnxType="I")
outB = b.addPort("out", cnxType="O")

endNode = view.addNode("End", bgColor=QtGui.QColor(0.6*255, 0.1*255, 0.3*255, 1.0*255))
inEnd1 = endNode.addPort("in1", cnxType="I")
inEnd2 = endNode.addPort("in2", cnxType="I")

outA.connectTo(inEnd1)
outB.connectTo(inEnd2)

view.reorderGraph()

## Un-comment if you don't want the user to edit the graph
# view.lock()

view.show()

sys.exit(app.exec_())

```

It will create this simple graph:

![Hello Vertical Graph](https://bytebucket.org/GuillaumeLaforge/dagui/raw/d9504d9e5474b2f84afd832601f83c5d389c0e58/doc/images/verticalGraph01.png?token=77b7647f56cac024a950fa5a21be28ebde9dd152)

If you want to create an horizontal graph (the default one actually), create a View instance like that:

```python
view = View(scene, 
	parent=None, 
	title="Hello Horizontal Graph",
	orientation="Horizontal"
)
```

![Hello Horizontal Graph](https://bytebucket.org/GuillaumeLaforge/dagui/raw/d9504d9e5474b2f84afd832601f83c5d389c0e58/doc/images/horizontalGraph01.png?token=cdc2afa48ba068101039273847ad42a5dd70ad0b)
